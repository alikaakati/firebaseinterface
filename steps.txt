First of all we set up the firebase console (Firebase is a google service-like API that allows us to Connect to a realtime database that we can retrieve and send data to easily)
The implementation is done by following the steps in the firebase documentations (Copying the config of the project to our web interface and passing them to the javascript part)
We create a graphical web interface that contains a text input field and a submit button
when we click on the submit button it retrieves the input button's text and sends them to the firebase
Each sentence will generate its valid unique key so we can retrieve them later
The firebase saves the information in a node like tree (KEY - VALUE).
